+++++++++++++++++

Spirent Traffic and Application Testing traversing a DUT/SUT into an emulated network
Proof of Concept User Summary:
•	User: Nikki Gilbert
•	Access Dates: 10/16/23 – 12/31/23
•	Access Times: 24H (user has access to testbed for 24h per day during access dates)
•	Access Level: Full (user can create, configure & run tests)
Description: Test & Measurement Capabilities
In this testbed, we are emulating stateful traffic patterns, which are going to be subject to real world impairments (like jitter, delay, latency, etc.) while rendering them over network elements. 
POC Test criteria:
•	Connecting DUT/SUT, attaching to a network, verifying IP address assignment, and the DUT accepts the traffic as sent
•	Passing IP traffic end-to-end without impairments, and documenting baseline performance.  Verifying DUT processes and accepts traffic at rates specified from 0 to 100% line rate.
•	Passing IP traffic end-to-end with impairments (bad, worse, and worst), and documenting performance related to customer requirements
POC Milestones:
•	Incorporating TBD custom traffic patterns with customer defined requirements
Challenges:
•	Testing a real DUT/SUT with numerous real-world traffic patterns
•	Keeping testing cycle short enough, yet yielding high test scenario execution
Customer Benefits:
•	Gain confidence that STC will deliver the expected tests
Solution Elements:
•	TestCenter 
+++++++++++++++++




**Spirent Traffic and Application Testing traversing a 5G DUT into an emulated 5G network**
#

**Proof of Concept User Summary:**
 - User: wncuser
 - Access Dates: 10/20/22 - 10/31/22
 - Access Times: 24H (user has access to testbed for 24h per day during access dates)
 - Access Level: Full (user can create, configure & run tests)

**SmartQoS Router Testing**

In this testbed, we are emulating stateful traffic patterns(like Netflix, Spotify, etc.), emulating normal home traffic. Traffic must be recognized by SmartQoS engine and fall into four target profiles - highest (gaming), high (AV conference), mediumd (streaming), low (web traffic). System should be able to characterize latency, jitter, packet loss, goodput
 
#
**POC Test criteria:**
 - Connecting a 5G DUT, attaching it to the 4G/5G network, and verifying IP address assignment
 - Passing IP traffic end-to-end without impairments, and documenting baseline performance
 - Passing IP traffic end-to-end with impairments (bad, worse, and worst), and documenting performance related to customer requirements

**POC Milestones:**
 - On-boarding the device in the lab and connecting it to the emulated cellular network
 - Validating sample pre-canned traffic patterns
 - Incorporating 2 custom traffic patterns with customer defined requirements


**Challenges:**
 - Testing a real DUT with numerous real-world traffic patterns
 - Keeping testing cycle short enough, yet yielding high test scenario execution
 - Testing real DUT across national and international cellular network profiles
 - Introducing adversarial scenario testing to harden DUT system design


**Customer Benefits:**
 - Gain confidence that your 5G UE design will deliver the expected performance
 - Quickly isolate performance issues to specific areas and vendors in your protocol stack
 - Validate cloud services with high load, realistic traffic, and peer device emulation for 5G on numerous real-world national/international CSPs


**Solution Elements:**
 - CyberFlood
 - Spirent Network Emulator
 - Velocity
 - Spirent 8100
 - Spirent Landslide 
#



----
----

#### Content

| Links |
| ------ |
| [Cyberflood Virtual](https://www.spirent.com/assets/u/cyberfloodvirtual_datasheet) |
| [TestCenter Virtual](https://www.spirent.com/assets/u/stc_virtual_datasheet) |

